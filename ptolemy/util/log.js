const { aristeas, aql } = require('./connect.js')

module.exports = {
  log: async function({ req, msg, e }) {
    if (e) er(req, e)
    else if (msg) ms(req, msg)
  },
}

async function er(req, e) {
  let msg
  if (e.response && e.response.body && e.response.body.errorMessage) {
    msg = e.response.body.errorMessage
  } else {
    msg = e.toString()
  }
  const err = {
    time: new Date(Date.now()),
    host: req && req.headers.host,
    url: req && req.url,
    method: req && req.method,
    msg,
  }
  await aristeas.query(aql`INSERT ${err} IN errors`).catch(e => {
    console.error('err: log.js ..er', e)
  })
}

async function ms(req, msg) {
  msg = {
    time: new Date(Date.now()),
    host: req && req.headers.host,
    url: req && req.url,
    method: req && req.method,
    msg,
  }
  await aristeas.query(aql`INSERT ${msg} IN log`).catch(e => {
    console.error('err: log.js ..ms', msg)
  })
}
