const fs = require('fs')
const path = require('path')

module.exports = {
  handleBody: async function(req) {
    let data = ''
    return new Promise((keep, rej) => {
      req.on('data', datum => {
        data += datum
        if (data.length > 1e7) {
          rej(new Error('malformed request'))
        }
      })
      req.on('end', () => {
        if (!data.length) {
          rej(new Error('malformed request'))
        }
        try {
          data = JSON.parse(data)
          keep(data)
        } catch (e) {
          rej(new Error(e))
        }
      })
    })
  },

  writeJSON: function(res, obj) {
    if (res.finished) return
    const str = JSON.stringify(obj)
    res.writeHead(200, {
      // 'Content-Type': 'application/json',
      'Content-Type': 'application/octet-stream',
      'Content-Length': Buffer.byteLength(str),
    })
    res.write(Buffer.from(str))
    res.end()
  },

  config: (() => {
    const default_path = path.resolve(
      __dirname,
      '../ptolemy-config-default.json',
    )
    const config_path = path.resolve(__dirname, '../ptolemy-config.json')
    if (fs.existsSync(config_path)) {
      return require(config_path)
    } else if (fs.existsSync(default_path)) {
      console.log('using default config..')
      return require(default_path)
    }
  })(),
}
