class ApiError extends Error {
  constructor(res, statusCode, message) {
    super(res, statusCode, message)
    this.res = res
    this.statusCode = statusCode
    this.message = message
  }
}

function handleError(res, status, msg) {
  if (!res || res.finished) return
  if (isNaN(status)) status = 500
  if (!msg) msg = 'err'

  res.writeHead(status, { 'Content-Type': 'application/json' })
  res.write(
    JSON.stringify({
      status,
      msg,
    }),
  )
  res.end()
}

module.exports = {
  ApiError,
  handleError,
}
