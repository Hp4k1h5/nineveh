const arangojs = require('arangojs')
const aql = arangojs.aql

const { config } = require('./helpers.js')
const { PTOLEMY_USER_ENV, PTOLEMY_PASS_ENV, arango_url, api_port } = config
const user = process.env[PTOLEMY_USER_ENV]
const pass = process.env[PTOLEMY_PASS_ENV]

class arango {
  constructor(dbName, user, pass) {
    this.cnx = new arangojs.Database({
      url: arango_url || 'tcp://localhost:8529',
    })
    this.dbName = dbName
    this.user = user
    this.pass = pass
  }

  createDB(name = this.dbName) {
    db.createDatabase(name)
  }

  getDB(dbName = this.dbName) {
    this.cnx.useDatabase(dbName)
    this.setAuth()
    return this.cnx
  }

  setAuth(user = this.user, pass = this.pass) {
    this.cnx.useBasicAuth(user, pass)
  }
}

// TODO find a way to run this check synchronously
// see https://www.arangodb.com/arangodb-user-management/
// check user's dbs
//const db = new arangojs.Database()
//db.useBasicAuth(user, pass)
// const dbs = await db.listUserDatabases()
// if (!dbs || !dbs.includes('alexander', 'ptolemy')) {
//   console.error(`your arangodb user must have full read/write access to 'alexander' and 'ptolemy' databases
//     please run the following commands from the root directory of this project
//     cat dimitri/indexer.js | arangosh --server.username YOUR_USERNAME --server.password YOUR_PASSWORD
//     cat dimitri/administrator.js`)
//   process.exit()
// }

// set up log connection
const cnx_aristeas = new arango('aristeas', user, pass)
const aristeas = cnx_aristeas.getDB()

// set up auth connection
const cnx_alexander = new arango('alexander', user, pass)
const alexander = cnx_alexander.getDB()

// set up search connection
const cnx_ptolemy = new arango('ptolemy', user, pass)
const ptolemy = cnx_ptolemy.getDB()

module.exports = { aristeas, ptolemy, alexander, aql }
