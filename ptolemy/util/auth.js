const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const { config } = require('./helpers.js')
const { ApiError } = require('./errors.js')

exports.makeHash = async function(pw) {
  return await bcrypt.hash(pw, 10)
}

exports.validPassword = async function(password, pass_hash) {
  const valid = await bcrypt.compare(password, pass_hash)
  if (!valid) {
    throw new Error('invalid login :: (vP001)')
  }
}

exports.createJWT = function(id, email, name) {
  return jwt.sign({ id, email, name }, config.jwt_secret, {
    expiresIn: '12h',
  })
}

exports.checkToken = function(req, res) {
  try {
    const token = req.headers.authorization.substring(7)
    const decoded = jwt.verify(token, config.jwt_secret)
    return { token, decoded }
  } catch (err) {
    throw new ApiError(res, 401, 'invalid token, try logging in again')
  }
}
