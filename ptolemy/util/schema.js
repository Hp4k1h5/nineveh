const fs = require('fs')
const path = require('path')
const YAML = require('yaml')
const { config } = require('./helpers.js')

let schema = fs.readFileSync(
  path.resolve(__dirname, '../' + config.schema_path),
  'utf8',
)

schema = YAML.parse(schema)
module.exports = { schema }
