const fs = require('fs')
const path = require('path')
const readline = require('readline')
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
})

const { ptolemy } = require('../util/connect.js')
const { parser } = require('./parsers/parseG.js')

// handle cl args
let dirname = process.argv[2]
if (!dirname) {
  // default to entire data directory
  dirname = ''
}

// @@ crawDirs
// params:
//   - dirname: the name of the directory or file inside the data directory to crawl
//   - parse: async callback, should return a json obj
//   - insert: async callback, aql insert query, no return
// called here
crawlDirs(dirname, parser)
async function crawlDirs(dirname, parse) {
  const dirpath = path.resolve(__dirname, 'data/', dirname)
  // recursive directory crawler
  let skip = 0
  async function crawl(dir, parse) {
    let stats
    try {
      stats = fs.statSync(dir)
    } catch (e) {
      console.error(`err: couldn't process... ${dir}`)
      return
    }
    // handle files
    if (stats.isFile(dir)) {
      while (skip++ < 0) {
        console.log('skip', skip)
        return
      }
      let doc
      doc = await parse(dir, ptolemy).catch(e => {
        console.error('err isfile', dir, e)
        doc = null
      })
      //  await qu('next')
      if (!doc) return

      // if doc ids exist update those
      if (doc.ids && doc.ids.length) {
        await updateDoc(doc).catch(e => {
          console.error(e)
        })
        return
      }
      await insertDoc(doc).catch(e => {
        console.error(e)
      })
      return
    }
    // otherwise add a document to each language collection
    const dirs = fs.readdirSync(dir)
    for (let d = 0; d < dirs.length; d++) {
      // recurse down dirs
      await crawl(path.resolve(dir, dirs[d]), parse)
    }
  }
  // call crawl
  await crawl(dirpath, parse)
}

async function qu(q) {
  let response
  rl.setPrompt(q)
  rl.prompt()
  return new Promise((resolve, reject) => {
    rl.on('line', userInput => {
      response = userInput
      resolve(response)
    })
    rl.on('close', () => {})
  })
}

async function insertDoc(doc) {
  if (!doc.langs || !doc.langs.length) doc.langs = ['en']
  console.log('inserting', doc.langs, doc.title)
  for (let i in doc.langs) {
    let colName = `${doc.langs[i]}_documents`
    let col = ptolemy.collection(colName)
    const exists = await col.exists()
    if (!exists) {
      console.log('creating col', colName)
      await col.create()
    }
    await ptolemy.query(`INSERT @doc INTO @@colName`, {
      doc,
      '@colName': colName,
    })
  }
}

async function updateDoc(doc) {
  for (let i in doc.ids) {
    doc._id = doc.ids[i]
    delete doc.bytes
    await ptolemy.query(
      `FOR doc IN @@col UPDATE {_key: @key} WITH @doc IN @@col`,
      {
        '@col': doc.ids[i].split('/')[0],
        key: doc.ids[i].split('/')[1],
        doc,
      },
    )
  }
}
