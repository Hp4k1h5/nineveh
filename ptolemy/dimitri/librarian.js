const fs = require('fs')
const path = require('path')

const { ptolemy, aql } = require('../util/connect.js')

;(async function() {
  const collections = await ptolemy.collections()
  if (!collections.find(c => c.name === 'stats')) {
    await ptolemy.collection('stats').create()
  }

  const totals = {}
  for (let col in collections) {
    let c = collections[col]
    const count = await c.count()
    totals[c.name] = count.count
  }

  const authors = []
  let res = await ptolemy.query(
    `FOR doc IN en_view
    COLLECT au = doc.author WITH COUNT INTO l
    SORT l DESC
    RETURN { au, l }`,
  )
  while (res.hasNext()) {
    const r = await res.next()
    authors.push(r)
  }
  const stats = {
    time: new Date(),
    totals,
    authors,
  }

  res = await ptolemy.query(aql`INSERT ${stats} INTO stats`)
  console.log(res)
})()
