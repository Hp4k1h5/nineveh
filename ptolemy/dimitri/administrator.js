;(async function() {
  const console = require('console')
  const internal = require('internal')
  const db = internal.db
  const dbs = db._databases()
  if (!dbs.includes('alexander')) {
    db._createDatabase('alexander')
  }
  db._useDatabase('alexander')

  // unique index on email field
  db.accounts.ensureIndex({ type: 'hash', fields: ['email'], unique: true })
  // insert test/test
  db.accounts.insert({
    email: 'rest',
    pass_hash: '$2b$10$h6OP8BiWQDIH2qXcKQOH8.Hz0zbNu/u4dVJPoJvMZJ3GV8WBrbhhm',
    name: 'Friend',
    created_at: '2020-01-21T14:24:41.691Z',
  })
})()
