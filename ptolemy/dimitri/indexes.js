const viewSchema = (viewName, colName, analyzers) => {
  let schema = {}
  schema[viewName] = {}
  schema[viewName][colName] = [
    {
      field: 'author',
      analyzers,
    },
    {
      field: 'authors',
      analyzers,
    },
    {
      field: 'title',
      analyzers,
    },
    {
      field: 'text',
      analyzers,
    },
    {
      field: 'md5',
      analyzers: ['identity'],
    },
  ]
  return schema
}

const indexes = collection => {
  return [
    {
      collection,
      type: 'identity',
      fields: ['md5'],
      unique: true,
      sparse: false,
    },
    {
      collection,
      type: 'fulltext',
      fields: ['filename', 'title', 'author', 'authors'],
      unique: false,
      sparse: false,
    },
  ]
}

function createIndexes(
  db,
  viewName = 'en_view',
  collection = 'en_documents',
  analyzers = ['text_en'],
) {
  const dbs = db.databases()
  if (!dbs.includes('ptolemy')) {
    db.createDatabase('ptolemy')
  }
  db.useDatabase('ptolemy')

  const viewSchemaKeys = Object.keys(
    viewSchema(viewName, collection, analyzers),
  )
  let collections = {}
  viewSchemaKeys.forEach(vk => {
    Object.keys(viewSchema[vk]).forEach(k => {
      collections[k] = true
    })
  })
  indexes.forEach(i => {
    collections[i.collection] = true
  })
  collections = Object.keys(collections)
  for (let c = 0; c < collections.length; c++) {
    try {
      db.createDocumentCollection(collections[c])
    } catch (e) {
      console.error('col exists', collections[c])
    }
  }

  // create Arango Search Views
  for (let i = 0; i < viewSchemaKeys.length; i++) {
    let viewName = viewSchemaKeys[i]
    let cols = viewSchema[viewSchemaKeys[i]]
    let _view = db._view(viewName)
    if (!_view) {
      try {
        _view = db.createView(viewName, 'arangosearch')
      } catch (e) {
        console.error('err creating view... ', viewName, e)
      }
    }
    Object.keys(cols).forEach(colKey => {
      const links = {}
      const col = cols[colKey]
      const fields = {}
      col.forEach(field => {
        fields[field.field] = { analyzers: field.analyzers }
      })
      links[colKey] = { includeAllFields: false, fields }
      _view.properties({ links })
    })
  }

  // create indexes
  for (let i = 0; i < indexes(collection).length; i++) {
    let index = indexes[i]
    db[index.collection].ensureIndex({
      type: index.type,
      fields: index.fields,
      unique: index.unique,
      sparse: index.sparse,
    })
  }
}

module.exports = {
  createIndexes,
}
