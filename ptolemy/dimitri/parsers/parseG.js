const fs = require('fs')
const path = require('path')
const crypto = require('crypto')

const FileType = require('file-type')
const cld = require('cld')
const pdf = require('pdf-parse')
const mammoth = require('mammoth')

// @@ parseG
// params:
//   - filename: '/'-separated string value of file location starting after the data directory
//               eg: 'file.pdf', 'subdir/file.txt', 'subdir/subdir/file.pdf'
// !! put files to parse in data directory
// general parser for .pdf and .txt filetypes
// returns plaintext, bytes, and best effort metadata
async function parseG(filename, _db) {
  let filepath
  // check file
  try {
    filepath = path.resolve(__dirname, '../data/', filename)
  } catch (e) {
    throw new Error(`err: can't find ${filename} in data dir, ${e}`)
  }

  // get bytes currently a let so we can destroy it if necessary
  let bytes = fs.readFileSync(filepath)
  let filetype = path.extname(filename)
  if (!['.pdf', '.txt', '.doc', '.docx'].includes(filetype)) {
    filetype = await FileType.fromBuffer(bytes).catch(e => {
      throw new Error(e)
    })
    filetype = '.' + filetype.ext
  }

  // check byte length
  const byte_len = bytes && Buffer.byteLength(bytes)
  if (!byte_len) {
    const msg = `err: nothing to read from : ${filename}`
    throw new Error(msg)
  }

  // only hash bytes
  const hash = crypto.createHash('md5')
  hash.update(bytes)
  const md5 = hash.digest('hex')
  if (!md5) {
    const msg = `err: no md5 hash, can't ingest ${filename}`
    throw new Error(msg)
  }

  // check db for hash
  let ids
  if (_db) {
    const exists = await _db.query({
      query: `
      FOR doc IN en_view
        SEARCH doc.md5 == @md5
      RETURN doc._id`,
      bindVars: {
        md5,
      },
    })
    if (exists._result[0]) {
      ids = exists._result
      //return
    }
  }

  let text,
    langs,
    title,
    author,
    authors,
    editors,
    lang,
    char_enc,
    date_pub,
    html

  // convert buffer to text
  if (filetype === '.pdf') {
    text = await pdf(bytes).catch(e => {
      const msg = `err: can't parse pdf ${filename}`
      throw new Error(msg)
    })
    // extract meta
    if (text.info) {
      title = text.info.Title
      author = text.info.Author
    }
    text = text.text
  } else if (filetype === '.txt') {
    // TODO: detect char encoding
    text = bytes.toString('utf8')
    // delete bytes for str based .txt files
    bytes = null
  } else if (['.doc', '.docx'].includes(filetype)) {
    try {
      text = await mammoth.extractRawText({
        buffer: bytes,
      })
      text = text && text.value
      html = await mammoth.convertToHtml({ buffer: bytes })
      html = html.value
    } catch (e) {
      console.error('err mammoth', e)
      throw e
    }
  }

  // extract meta
  // get langs
  langs = await getLangs(text).catch(e => {
    console.error('err detecting language', e)
  })

  // title
  const titleRegexes = [
    {
      re: new RegExp('^(?:.{1,500}?)title(?:s|d)?[-:\\s]{1,4}?(.{1,50})', 'is'),
      sub: 1,
    },
    {
      re: new RegExp('^(.{1,80})', 's'),
      sub: 1,
    },
    {
      re: new RegExp('^.{1,50}', 's'),
      sub: 0,
    },
  ]
  let i = 0
  while (text && !title && i < titleRegexes.length) {
    title = text.match(titleRegexes[i].re)
    title =
      title && title[titleRegexes[i].sub].replace(/[\r\n]+|\s{2,} /gs, ' ')
    title = title && title.trim()
    i++
  }
  if (!title) title = path.basename(filename)

  // author
  if (!author && text) {
    author = text.match(/author(?:\(s\))[:\\s]{1,4}(.{1,100})/i)
    author = author && author[1].replace(/[\r\n]+/gs, ' ').trim()
    if (!author) {
      author = text.match(/^(?:.{1,300})by[-:\s]+([^-:]{1,100})/is)
      author = author && author[1].trim()
    }
  }

  if (!editors && text) {
    editors = text.match(/^(?:.{1,1000})editors?[:\s]+(.{1,200})/i)
    editors = editors && editors[1].trim()
  }

  if (!lang && text) {
    lang = text.match(/^(?:.{1,1000})lang(?:uages?)?[:\s]+(.{1,30}\s{2}?)/is)
    lang = lang && lang[1].trim()
  }

  if (!char_enc && text) {
    char_enc = text.match(
      /^(?:.{1,1000})(?:char(?:acter)?)\s{0,4}(?:set)?\s{0,4}(?:encoding)?[:\s]+(.{1,10})/i,
    )
    char_enc = char_enc && char_enc[1].trim()
  }

  if (!date_pub && text) {
    date_pub = text.match(/^(?:.{1,1000})date[:\s]+(.{1,60})\s{2}?/is)
    date_pub = date_pub && date_pub[1].trim()
  }

  // url_src
  let url_src = text && text.match(/url[:\s]+?((http|www).{1,100})/i)
  url_src = url_src && url_src[1].trim()

  // src
  let src =
    text && text.match(/^(.{1,1000})?(source|src)[:\s]{1,2}(.{1,150})/is)
  src = src && src[2].trim()

  console.log({
    ids,
    md5,
    filename: path.basename(filename),
    filetype,
    langs,
    title,
    author,
    authors,
    editors,
    date_pub,
    lang,
    url_src,
    char_enc,
    src,
    byte_len,
  })
  const str = bytes && bytes.toString('latin1')
  return {
    ids,
    bytes: str,
    md5,
    filename,
    filetype,
    text,
    langs,
    html,
    title,
    author,
    authors,
    editors,
    date_pub,
    lang,
    url_src,
    char_enc,
    src,
    byte_len,
  }
}
module.exports = { parser: parseG }
//parseG('G/franklin_kinyras-divine-lyre.pdf')
//parseG('I/dickerman_the-hittites.pdf')
//parseG('Y/berens_myths-and-legends.txt')
//parseG('G/mousenkis_greek-society-in-linear-a.docx')
//parseG('G/mousenkis_flourishing-of-minoan-greek-state.docx')
//parseG('D/Aegaeum37_Yasur_Landau_Goshen.libre.pdf')
//parseG('D/Aneziri1994.pdf')
//parseG('D/Aristoxenus_and_the.Neoclassicists.pdf')
//parseG('D/Barnett1953Mopsos.pdf')

async function getLangs(text) {
  return new Promise((keep, brk) => {
    cld.detect(text, (err, langs) => {
      if (err) brk(err)
      langs = langs.languages.filter(l => l.percent > 5).map(l => l.code)
      keep(langs)
    })
  })
}
