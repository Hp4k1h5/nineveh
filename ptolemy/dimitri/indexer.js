// an arangosh utility script to create views and indices
// `cat indexer.js | arangosh`

const console = require('console')
const internal = require('internal')

const viewSchema = (viewName, colName, analyzers) => {
  let schema = {}
  schema[viewName] = {}
  schema[viewName][colName] = [
    {
      field: 'author',
      analyzers,
    },
    {
      field: 'authors',
      analyzers,
    },
    {
      field: 'title',
      analyzers,
    },
    {
      field: 'text',
      analyzers,
    },
    {
      field: 'md5',
      analyzers: ['identity'],
    },
  ]
  return schema
}

const indexes = collection => {
  return [
    {
      collection,
      type: 'persistent',
      fields: ['md5'],
      unique: true,
      deduplicate: true,
      sparse: false,
    },
    {
      collection,
      type: 'fulltext',
      fields: ['filename'],
      unique: false,
    },
    {
      collection,
      type: 'fulltext',
      fields: ['title'],
      unique: false,
    },
    {
      collection,
      type: 'fulltext',
      fields: ['author'],
      unique: false,
    },
    {
      collection,
      type: 'fulltext',
      fields: ['authors'],
      unique: false,
    },
  ]
}
// missing [ 'el' 'la' 'tr' ] until we get the snowball analyzers
const availLangs = ['en', 'fr', 'de', 'it', 'es']

;(function createIndexes(db = internal.db, viewName = 'en_view') {
  const dbs = db._databases()
  if (!dbs.includes('ptolemy')) {
    db._createDatabase('ptolemy')
  }
  db._useDatabase('ptolemy')

  for (let l = 0; l < availLangs.length; l++) {
    let collection = availLangs[l] + '_documents'
    let analyzers = ['text_' + availLangs[l]]

    // get unique collection names
    const vs = viewSchema(viewName, collection, analyzers)
    const viewSchemaKeys = Object.keys(vs)
    let collections = {}
    viewSchemaKeys.forEach(vk => {
      Object.keys(vs[vk]).forEach(k => {
        collections[k] = true
      })
    })
    const is = indexes(collection)
    is.forEach(i => {
      collections[i.collection] = true
    })

    // create collections that dont exist...
    // TODO: find a way that doesnt involve try/catch
    const colls = Object.keys(collections)
    for (let c = 0; c < colls.length; c++) {
      try {
        db._createDocumentCollection(colls[c])
      } catch (e) {
        console.error('col exists', colls[c])
      }
    }

    // create Arango Search Views
    for (let i = 0; i < viewSchemaKeys.length; i++) {
      let viewName = viewSchemaKeys[i]
      let cols = vs[viewSchemaKeys[i]]
      let _view = db._view(viewName)
      if (!_view) {
        try {
          _view = db._createView(viewName, 'arangosearch')
        } catch (e) {
          console.error('err creating view... ', viewName, e)
        }
      }
      Object.keys(cols).forEach(colKey => {
        const links = {}
        const col = cols[colKey]
        const fields = {}
        col.forEach(field => {
          fields[field.field] = { analyzers: field.analyzers }
        })
        links[colKey] = {
          includeAllFields: false,
          primarySort: [{ field: 'md5', direction: 'asc' }],
          fields,
        }
        _view.properties({ links })
      })
    }

    // create indexes
    for (let i = 0; i < is.length; i++) {
      let index = is[i]
      try {
        db[index.collection].ensureIndex({
          type: index.type,
          fields: index.fields,
          unique: index.unique,
        })
      } catch (e) {
        console.error(e)
      }
    }
  }
})()
