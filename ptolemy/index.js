const os = require('os')
const fs = require('fs')
const https = require('https')
const url = require('url')

const routes = require('./endpoints/endpoints.js')
const { config } = require('./util/helpers.js')
const { handleError } = require('./util/errors.js')
const { log } = require('./util/log.js')

const options = {
  key: fs.readFileSync(config.ssl_key_path),
  cert: fs.readFileSync(config.ssl_cert_path),
}

;(async function() {
  https
    .createServer(options, async (req, res) => {
      setHeaders(req, res)
      if (res.finished) {
        return
      }
      // TODO:
      // handle route and url params
      const routeObj = handleRoute(req, res)
      if (!routeObj) {
        handleError(res, 404, 'not found')
        return
      }
      const { path, method, params } = routeObj
      // call endpoint
      routes[path][method](...params).catch(err => {
        handleError(err.res, err.statusCode || 500, err.message)
      })
    })
    .listen(config.api_port, () => {
      // log api instantiation
      log({
        msg: {
          event: 'api start',
          arch: os.arch(),
          cpus: os.cpus(),
          platform: os.platform(),
          mem: os.totalmem(),
        },
      })

      console.log(`listening on port ${config.api_port}`)
    })
})()

// helper functions
function handleRoute(req, res) {
  // instantiate request objects
  const method = req.method
  let { pathname, query } = url.parse(req.url, true)
  let [path, ...subpath] = pathname.split('/').slice(1)
  const params = [req, res, query]

  // handle subpath and url paramters
  let routeKeys = Object.keys(routes)
    .map(k => k.split('/'))
    .filter(k => k[0] === path && k.length === subpath.length + 1)

  subpath.forEach((p, pi) => {
    for (let ki = 0; ki < routeKeys.length; ki++) {
      if (routeKeys[ki][pi + 1][0] === ':') {
        routeKeys = routeKeys.filter(k => k[pi + 1][0] === ':')
        params.splice(-1, 0, p)
        break
      } else if (routeKeys[ki][pi + 1] !== p) {
        routeKeys.splice(ki, 1)
      } else if (routeKeys.find(k => k[pi + 1] === p)) {
        routeKeys = routeKeys.filter(k => k[pi + 1] === p)
        break
      }
    }
  })

  if (routeKeys[0]) {
    path = routeKeys[0].join('/')
  }
  if (routeKeys.length !== 1 || !routes[path] || !routes[path][method]) {
    return
  }

  return {
    path,
    method,
    params,
  }
}

function setHeaders(req, res) {
  if (req.method === 'OPTIONS') {
    let headers = {}
    // IE8 does not allow domains to be specified, just the *
    // headers["Access-Control-Allow-Origin"] = req.headers.origin;
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] =
      'POST, GET, PATCH, PUT, DELETE, OPTIONS'
    headers['Access-Control-Allow-Credentials'] = false
    headers['Access-Control-Max-Age'] = '86400' // 24 hours
    headers['Access-Control-Allow-Headers'] =
      'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Authorization'
    res.writeHead(200, headers)
    res.end()
    return
  }
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Request-Method', '*')
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Content-Type, Authorization, TE',
  )
}
