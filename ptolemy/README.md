#### !!! Work in Progress

## ptolemy

the backend/api for the nineveh digital library

- contents
  - [setup / development](#development-setup)
  - [instructions](#instructions)
  - [dimitri](#dimitri)
  - [deploying](#deploying)

### development setup

the `ptolemy` folder contains the api and its dependencies.

- `dimitri` is a data ingest/parse/index/catalog utility sub-package.
- `aristotle` is the optional testing suite.

##### dependencies

- [arangodb](https://www.arangodb.com/)
- [nodejs](https://nodejs.org/)
- [npm](https://www.npmjs.com/)/[yarn](https://yarnpkg.com/)
  - [arangojs](https://www.npmjs.com/package/arangojs)
  - [bcrypt](https://www.npmjs.com/package/bcrypt)
  - [cld](https://www.npmjs.com/packagea/cld)
  - [file-type](https://www.npmjs.com/packagea/file-type)
  - [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)
  - [yaml](https://www.npmjs.com/package/yaml)

###### dimitri dependencies

- [pdf-parse](https://www.npmjs.com/package/pdf-parse)
- [mammoth](https://www.npmjs.com/package/mammoth)

###### aristotle dependencies

- [@hp4k1h5/rek](https://www.npmjs.com/package/@hp4k1h5/rek)
- [@hp4k1h5/t](https://www.npmjs.com/package/@hp4k1h5/t)

#### instructions:

1. ##### spin up an arangod instance
2. ##### edit `ptolemy-config.js`
   export your env vars; either use the right-hand side env var names or modify the config

- `PTOLEMY_USER_ENV` : your env var name masking the arangodb user with authorization to create and modify dbs
- `PTOLEMY_PASS_ENV` : your env var name masking the arangodb pass for `PTOLEMY_USER_ENV`-linked env var

3. ##### create required elements

   a) dbs, collections, indices,

   - `cat ptolemy/dimitri/indexer.js | arangosh --server.username YOUR_USERNAME --server.password YOUR_PASSWORD`
     - ! if your arangod instance is not `tcp://localhost:8529` you will need to add the endpoint by adding `--server.endpoint https://yourendpoint.com:8529`

   b) users

   - `cat ptolemy/dimitri/administrator.js | arangosh --server.username YOUR_USERNAME --server.password YOUR_PASSWORD`

4. ##### run

   a) dev

   - start the api by running `yarn ptolemy`

   b) prod

   - git push origin/master

#### dimitri

dimitri is a setup/ingest/clean/parse/index/catalog utility for ptolemy

- `indexer.js` is an arangosh tool to create default dbs and their associated indexes and views. these settings can be altered in the `indexer.js` file itself. this should be run after spinning up an arangod instance and setting config variables and env variables, per instructions above. one method of running this script by piping the file into an arangosh program is detailed above. you can also import this file into the arangosh in the cli and run it there.
  - usage: `cat ptolemy/dimitri/indexer.js | arangosh --server.username YOUR_USERNAME --server.password YOUR_PASSWORD`
- `curator.js` is a file parser and ingester. you can pass a single file as a parameter or any directory residing inside the `data` directory inside dimitri.
  - usage: `node curator [file/directory] [type]`

##### example commands

if your dimitri/data directory looks like this:

- dimitri
  - data
    - books
      - book.pdf
      - book2.pdf
    - journals
      - article.docx
      - article.pdf
    - document1.txt
    - document2.docx

you can use any of the following commands to ingest these documents:

##### ... parse and ingest the `document1.txt`

```bash
node curator document1.txt
```

##### ... parse and ingest the entire `books` subdirectory and all recursively found subdirectories

```bash
node curator books
```

##### ... parse and ingest the entire `data` directory and all recursively found subdirectories

```bash
node curator
```
