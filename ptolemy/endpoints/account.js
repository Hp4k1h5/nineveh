const { alexander, ptolemy, aql } = require('../util/connect.js')
const { handleBody, writeJSON } = require('../util/helpers.js')
const { ApiError } = require('../util/errors.js')
const { log } = require('../util/log.js')
const {
  makeHash,
  validPassword,
  createJWT,
  checkToken,
} = require('../util/auth.js')
const { schema } = require('../util/schema.js')

module.exports = {
  account: {
    // CREATE USER ACCOUNT
    POST: async function(req, res) {
      const data = await handleBody(req).catch(e => {
        throw new ApiError(res, 400, "can't parse json")
      })
      const pass_hash = await makeHash(data.password)
      const account = {
        name: data.name,
        email: data.email,
        pass_hash,
        created_at: new Date(Date.now()),
        saved_queries: [],
      }

      await alexander
        .query(`INSERT @account IN accounts RETURN NEW`, { account })
        .catch(e => {
          let msg =
            e.errorNum === 1210
              ? `account ${account.email} already exists`
              : e.response.body.errorMessage
          log({ req, e })
          throw new ApiError(res, e.statusCode, msg)
        })
      writeJSON(res, { msg: `user ${account.email} created` })
    },
  },

  login: {
    // LOGIN
    GET: async function(req, res, params) {
      if (!params || !['email', 'password'].every(key => params[key]))
        throw new ApiError(res, 401, 'invalid login')
      const { email, password } = params
      // find email
      const account = await alexander
        .query(
          'FOR account IN accounts FILTER account.email == @email RETURN account',
          { email },
        )
        .catch(e => {
          log({ req, e })
          throw new ApiError(res, 401, 'invalid login')
        })

      // check password
      if (!account._result || !account._result.length) {
        throw new ApiError(res, 401, 'invalid login')
      }
      await validPassword(password, account._result[0].pass_hash).catch(() => {
        throw new ApiError(res, 401, 'invalid login')
      })

      // get schema
      const { dbs } = getAccountSchema(schema, account)

      // return user object
      const jwt = createJWT(
        account._result[0]._id,
        email,
        account._result[0].name,
      )
      writeJSON(res, {
        jwt,
        name: account._result[0].name,
        email,
        dbs,
        saved_queries: account._result[0].saved_queries,
      })
    },
  },

  refresh: {
    // REFRESH
    POST: async function(req, res) {
      const tokens = checkToken(req, res)
      const account = await alexander
        .query(
          aql`FOR account IN accounts FILTER account.email == ${tokens.decoded.email} RETURN account`,
        )
        .catch(e => {
          log({ req, e })
          throw new ApiError(res, 500, e)
        })

      const { dbs } = getAccountSchema(schema, account)

      // return user object
      writeJSON(res, {
        jwt: tokens.token,
        name: account._result[0].name,
        email: account._result[0].email,
        dbs,
        saved_queries: account._result[0].saved_queries,
      })
    },
  },
}

function getAccountSchema(schema, account) {
  if (!account._result[0].dbs) {
    return { dbs: [] }
  }
  const dbs = schema.databases.filter(db =>
    account._result[0].dbs.includes(db.name),
  )
  return {
    dbs,
  }
}
