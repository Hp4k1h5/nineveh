const { ptolemy } = require('../util/connect.js')
const { handleBody, writeJSON } = require('../util/helpers.js')
const { ApiError } = require('../util/errors.js')
const { checkToken } = require('../util/auth.js')

module.exports = {
  stats: {
    GET: async function(req, res) {
      const tokens = checkToken(req, res)

      const stats = await ptolemy
        .query(
          `FOR stat IN stats 
          SORT stat.time DESC 
          LIMIT 1 
          RETURN stat`,
        )
        .catch(e => {
          throw new ApiError(res, 403, e)
        })
      writeJSON(res, stats._result[0])
    },
  },
}
