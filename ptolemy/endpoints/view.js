const { ptolemy } = require('../util/connect.js')
const { handleBody, writeJSON } = require('../util/helpers.js')
const { ApiError } = require('../util/errors.js')
const { log } = require('../util/log.js')
const { checkToken } = require('../util/auth.js')

module.exports = {
  'view/:col/:_key': {
    GET: async function(req, res, col, _key) {
      const tokens = checkToken(req, res)
      const c = ptolemy.collection(col)
      const doc = await c.document({ _id: `${col}/${_key}` }).catch(e => {
        log({ req, e })
        throw new ApiError(res, 500, `err: no doc`)
      })
      await writeJSON(res, doc)
    },
  },
}
