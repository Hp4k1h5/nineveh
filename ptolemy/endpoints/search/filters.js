const { aql } = require('./../../util/connect.js')

module.exports = {
  buildFilters(filters) {
    if (!filters.length) return
    filters = filters.map(
      f => aql`doc.${f.field} ${aql.literal(f.op)} ${f.val}`,
    )
    filters = aql.join(filters, '&&')
    return aql`FILTER ${filters}`
  },
}
