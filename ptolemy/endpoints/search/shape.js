async function shapeResults(cursor, data) {
  const results = []
  let queryRegex
  if (data.hl) {
    queryRegex = data.queryArr
      .filter(q => q.op !== '-')
      .map(q =>
        q.val
          .replace(/["'()]/g, '')
          .replace(/[#-.]|[[-^]|[?|{}]/g, '\\$&')
          .trim(),
      )
      .join('\\W|\\W')
    queryRegex = queryRegex && new RegExp(queryRegex, 'ig')
  }

  while (cursor.hasNext()) {
    const res = await cursor.next()
    let resMatches = []
    let matchCount = 0
    let resArr
    if (res.text && data.hl && queryRegex) {
      while (
        matchCount++ < 10 &&
        (resArr = queryRegex.exec(res.text)) !== null
      ) {
        const end = queryRegex.lastIndex
        const start = end - resArr[0].length
        resMatches.push(
          `${res.text.substring(
            start - 30,
            start,
          )}<span style="font-weight:bold">${res.text.substring(
            start,
            end,
          )}</span>${res.text.substring(end, end + 50)}`,
        )
      }
      res.text = resMatches.join(
        '<span style="font-size:18px; font-weight: bold;"> ][ </span>',
      )
    } else {
      res.text = res.text ? res.text.substring(0, 700) : ''
    }
    results.push(res)
  }
  return results
}

module.exports = { shapeResults }
