const { alexander, ptolemy, aql } = require('../../util/connect.js')
const { handleBody, writeJSON } = require('../../util/helpers.js')
const { ApiError } = require('../../util/errors.js')
const { checkToken } = require('../../util/auth.js')
const { log } = require('../../util/log.js')

module.exports = {
  'search/save': {
    POST: async function(req, res) {
      const tokens = checkToken(req, res)
      const data = await handleBody(req)

      // update db
      let saved_queries = await alexander
        .query(
          aql`
        LET a = DOCUMENT(${tokens.decoded.id})
        LET qs = SLICE(UNSHIFT(a.saved_queries, ${data}), 0, 20)
        FOR account IN accounts
          UPDATE {_key: a._key}
          WITH { saved_queries: qs }
          IN accounts
        RETURN NEW.saved_queries`,
        )
        .catch(e => {
          log({ req, e })
          throw new ApiError(res, 500, e.toString())
        })

      saved_queries = await saved_queries.next()
      // write back updated array
      writeJSON(res, saved_queries)
    },
  },

  'search/saved/:i': {
    DELETE: async function(req, res, i) {
      const tokens = checkToken(req, res)

      await alexander
        .query(
          aql`
        LET a = DOCUMENT(${tokens.decoded.id})
        FOR account IN accounts 
          UPDATE {_key: a._key}
          WITH {
            saved_queries: REMOVE_NTH(a.saved_queries, ${i})
          }
          IN accounts`,
        )
        .catch(e => {
          log({ req, e })
          throw ApiError(res, 501, e.toString())
        })
      writeJSON(res, { msg: 'ok' })
    },

    PATCH: async function(req, res, i) {
      const tokens = checkToken(req, res)
      const data = await handleBody(req)

      let queries = await alexander
        .query(
          aql`
          LET a = DOCUMENT(${tokens.decoded.id})
          FOR account IN accounts
            UPDATE {_key: a._key}
            WITH {
              saved_queries: ${data}
            }
            IN accounts
          RETURN NEW.saved_queries`,
        )
        .catch(e => {
          log({ req, e })
          throw ApiError(res, 501, e.toString())
        })
      queries = await queries.next()
      writeJSON(res, queries)
    },
  },
}
