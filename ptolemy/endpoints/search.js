const { alexander, ptolemy, aql } = require('../util/connect.js')
const { handleBody, writeJSON } = require('../util/helpers.js')
const { log } = require('../util/log.js')
const { ApiError } = require('../util/errors.js')
const { checkToken } = require('../util/auth.js')
const saved = require('./search/saved.js')
const { buildFilters } = require('./search/filters.js')
const { shapeResults } = require('./search/shape.js')

module.exports = {
  // first apply all sub modules
  // key (ie. route) collisions will mean the routes in this file overwriting those found in submodules
  ...saved,
  'search/l/:lang': {
    POST: async function(req, res, v_lang) {
      const tokens = checkToken(req, res)
      const data = await handleBody(req)
      // check cursor size
      if (data.end - data.start > 20) {
        throw new ApiError(res, 416, 'max 20 results per query')
      }
      // TODO: VET queryArr
      // build query
      const collections = await buildCols(data.collections)
      const SEARCH = buildSearch(data.queryArr, collections)
      const FILTER = buildFilters(data.filters)
      let _query = aql`
          FOR doc IN ${aql.literal(v_lang + '_view')}
            ${SEARCH}
            ${FILTER}
          LIMIT ${data.start}, ${data.end}
          COLLECT d = {
             md5: doc.md5,
             _id: doc._id,
             _key: doc._key,
             _rev: doc._rev,
             title: doc.title,
             filename: doc.filename,
             langs: doc.langs,
             author: doc.author,
             authors: doc.authors,
             date_pub: doc.date_pub,
             text: ${data.hl ? aql`doc.text` : aql`SUBSTRING(doc.text, 0, 300)`}
             }
           RETURN d`
      // query db
      const cursor = await ptolemy
        .query(_query, {
          count: true,
          options: {
            fullCount: true,
          },
        })
        .catch(e => {
          log({ req, e })
          throw new ApiError(res, e.statusCode, 'aql error')
        })

      // shape results
      const results = await shapeResults(cursor, data)

      writeJSON(res, {
        results,
        stats: cursor.extra.stats,
      })
    },
  },
}

function buildSearch(qA, collections) {
  let ANDS = buildOPS(qA, collections, '+')
  let ORS = buildOPS(qA, collections, '?')
  if (ANDS && ORS) {
    ANDS = aql`${ANDS} OR (${ANDS} AND ${ORS})`
    ORS = undefined
  }
  let NOTS = buildOPS(qA, collections, '-')
  if (!!NOTS) {
    NOTS = aql`${ANDS || ORS ? aql.literal(' AND ') : undefined} 
    ${NOTS.phrs ? aql.literal(' NOT ') : undefined} ${NOTS.phrs}
    ${NOTS.phrs && NOTS.anas ? aql.literal(' AND ') : undefined} ${NOTS.anas}`
  }

  const search = aql`
  SEARCH 
    ${ANDS} 
    ${ORS}
    ${NOTS}
    ${(!ANDS && !ORS && !NOTS) || undefined}
    OPTIONS ${{ collections }}
    SORT TFIDF(doc) DESC`
  return search
}

function buildOPS(q, c, op) {
  let o = op === '+' ? ' AND ' : 'OR '
  q = q.filter(qq => qq.op === op)
  if (!q.length) return

  // phrases
  let phrs = q
    .filter(qq => qq.type === 'phr')
    .map(p => {
      return buildPhrase(p, c)
    })
  if (!phrs.length) {
    phrs = undefined
  } else {
    phrs = aql.join(phrs, o)
  }

  // analyzers
  let anas = q.filter(qq => qq.type === 'ana')
  anas = anas && buildAnalyzer(anas, c, op)

  if (!anas && !phrs) return
  if (op === '-') {
    return { phrs, anas }
  }
  if (!phrs ^ !anas) {
    return !phrs ? anas : phrs
  }
  return aql.join([phrs, anas], o)
}

function buildPhrase(q, c) {
  const phrs = []
  c.forEach(col => {
    const l = 'text_' + col.split('_')[0]
    phrs.push(aql`PHRASE(doc.text, ${q.val.slice(1, -1)}, ${l})`)
  })
  return aql`(${aql.join(phrs, ' OR ')})`
}

function buildAnalyzer(q, collections) {
  if (!q.length) return
  const map = {
    '+': 'ALL',
    '?': 'ANY',
    '-': 'NONE',
  }
  let mapped = q.reduce((a, v) => {
    a[map[v.op]] = a[map[v.op]] ? a[map[v.op]] + ' ' + v.val : v.val
    return a
  }, {})

  const makeAnas = (tokens, op, a) => {
    return aql`
      ANALYZER(
        TOKENS(${tokens}, ${a})
        ${aql.literal(op)} IN doc.text, ${a})`
  }

  let remapped = []
  for (let i = 0; i < collections.length; i++) {
    let l = collections[i].split('_')[0]
    remapped.push(
      ...Object.keys(mapped).map(op => {
        return makeAnas(mapped[op], op, 'text_' + l)
      }),
    )
  }
  return aql`MIN_MATCH(${aql.join(remapped, ', ')}, ${
    q[0].op === '-' ? collections.length : 1
  })`
}

function buildProximity(q, arr) {}

async function buildCols(cols) {
  const view = ptolemy.arangoSearchView('en_view')
  const data = await view.properties()
  const c = cols.filter(col => Object.keys(data.links).includes(col))
  return (c.length && c) || ['en_documents']
}
