// routes
const account = require('./account.js')
const search = require('./search.js')
const view = require('./view.js')
const stats = require('./stats.js')

module.exports = {
  ...account,
  ...search,
  ...view,
  ...stats,
}
