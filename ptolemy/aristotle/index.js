const { t } = require('@hp4k1h5/t')
const { assert } = require('./util/config.js')
const t_api = require('./tests_ptolemy/test_api.js')
const t_routes = require('./tests_ptolemy/test_routes.js')
const t_account = require('./tests_ptolemy/test_account.js')
const tests = [...t_api, ...t_routes, ...t_account]

t(tests)
