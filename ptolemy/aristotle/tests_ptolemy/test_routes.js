const { assert, rek, apiAddress } = require('../util/config.js')

module.exports = [
  {
    name: '/ GET ?',
    fn: async () => {
      // / should respond with 404 not found
      const r = await rek.GET()
      assert.deepStrictEqual(
        r,
        { status: 404, msg: 'not found' },
        'GET / should respond with 404',
      )
    },
  },

  {
    name: '/login GET ?',
    fn: async () => {
      const tokens = await rek.GET(
        `${apiAddress}/login?email=test&password=test`,
      )

      const validKeys = ['jwt', 'email', 'dbs', 'saved_queries']
      // /login GET should respond with  valid tokens
      assert.deepStrictEqual(
        Object.keys(tokens),
        validKeys,
        `incorrect token keys\n.. exp: ${validKeys}\n.. rec: ${Object.keys(
          tokens,
        )}`,
      )
      // /login GET should respond with correct email
      assert.equal(
        tokens.email,
        'test',
        `incorrect email\n.. expected: 'test'\n.. received: ${tokens.email}`,
      )
    },
  },

  {
    name: '/login GET errors ?',
    fn: async () => {
      assert.doesNotReject(
        rek.GET(`${apiAddress}/login`),
        'invalid login throws',
      )
      const expected = { status: 401, msg: 'invalid login' }
      const r = await rek.GET(`${apiAddress}/login`)
      assert.deepStrictEqual(
        r,
        expected,
        `incorrect response.. \nreceived:\n${JSON.stringify(
          r,
          null,
          2,
        )}\n expected:\n${JSON.stringify(expected, null, 2)}`,
      )
    },
  },
]
