const { assert, rek, apiAddress } = require('../util/config.js')

module.exports = [
  {
    name: 'api up ?',
    fn: async () => {
      // api should respond to OPTIONS request
      await assert.doesNotReject(rek.OPTIONS(), 'api OPTIONS not responding')
      const o = await rek.OPTIONS()
      const validMethods = 'POST, GET, PATCH, PUT, DELETE, OPTIONS'
      // OPTIONS request should return valid methods
      assert.equal(
        o['access-control-allow-methods'],
        validMethods,
        `api OPTIONS not returning correct methods\n..exp: ${validMethods}\n..rec: ${o['access-control-allow-methods']}`,
      )
    },
  },

  {
    name: 'kanye ?',
    fn: async () => {
      let response = await rek.GET('https://api.kanye.rest')
      assert.ok(response, 'api not responding')
    },
  },
]
