const { assert, rek, apiAddress } = require('../util/config.js')
const { ApiError } = require('../../util/errors.js')

const account_endpoints = require('../../endpoints/account.js')

module.exports = [
  {
    name: 'account endpoints exist ?',
    fn: async () => {
      assert.ok(account_endpoints, 'account endpoints do not exist')
    },
  },

  {
    name: 'account throws if query params not present ?',
    fn: async () => {
      const r0 = async () =>
        await account_endpoints.login.GET(`${apiAddress}/login`)
      assert.rejects(
        r0,
        Error,
        'account does not throw if no/bad query params are present',
      )
    },
  },

  {
    name: 'test invalid creds ?',
    fn: async () => {
      const r1 = async () =>
        await account_endpoints.login.GET(
          `${apiAddress}/login?email=bad&password=invalid`,
        )
      assert.rejects(r1, Error, 'account does not throw if creds are invalid')
    },
  },

  {
    name: 'account rejects on bad password ?',
    fn: async () => {
      const r2 = async () =>
        await account_endpoints.login.GET(
          `${apiAddress}/login?email=test&password=invalid`,
        )
      assert.rejects(r2, Error, 'account does not throw if password is invalid')
    },
  },
]
