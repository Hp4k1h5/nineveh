const path = require('path')
const url = require('url')
const assert = require('assert')

const { Rek } = require('@hp4k1h5/rek')

const apiAddress =
  process.env.NODE_ENV === 'production'
    ? 'https://ptolemy.rumor.fun:3000'
    : 'https://localhost:3000'
const urlObj = url.parse(apiAddress)
const options = {
  hostname: urlObj.hostname || urlObj.href,
  path: urlObj.path,
  port: urlObj.port,
  query: new URL(apiAddress).search,
}

const rek = new Rek(options, true)

module.exports = { assert, rek, apiAddress }
