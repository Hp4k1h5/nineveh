import store from '../store'

class HTTP {
  constructor() {
    //    this.origin = window.location.origin + '/ptolemy'
    //    this.origin = 'https://ptolemy.rumor.fun:3000'
    this.origin = 'https://localhost:3000'
    this.headers = {}
  }

  async wrapRequest(endpoint, method, body) {
    const params = {
      method: method,
      headers: this.headers || {},
      body: body && JSON.stringify(body),
    }

    let response = await fetch(`${this.origin}/${endpoint}`, params)
    if (response.status !== 200) {
      store.dispatch('util/notify', { e: response.msg || response.statusText })
      if (store.state.account.jwt && response.status === 401) {
        store.dispatch('account/logout')
      }
      return
    }

    response = await prog(response)
    if (response.bytes) {
      response.bytes = str2ab(response.bytes)
    }

    try {
      return response
    } catch (e) {
      throw new ApiError(500, 'json error')
    }
  }
}

class Http extends HTTP {
  constructor() {
    super()
    this.headers['Content-Type'] = 'application/json'
  }
  async GET(endpoint) {
    return this.wrapRequest(endpoint, 'GET')
  }

  async DELETE(endpoint) {
    return this.wrapRequest(endpoint, 'DELETE')
  }

  async POST(endpoint, body) {
    return this.wrapRequest(endpoint, 'POST', body)
  }

  async PATCH(endpoint, body) {
    return this.wrapRequest(endpoint, 'PATCH', body)
  }
}

class AuthHttp extends Http {
  constructor() {
    super()
    this.setAuth()
  }
  setAuth() {
    this.jwt = localStorage.getItem('jwt')
    this.headers['Authorization'] = `Bearer ${this.jwt}`
  }
}

export const http = new Http()
export const auth = new AuthHttp()

export class ApiError extends Error {
  constructor(status, message) {
    super(status, message)
    this.status = status || 400
    this.message = message || 'error'
  }
}

// calculates percent of request received
async function prog(response) {
  store.state.search.viewProg = 0
  const reader = response.body.getReader()
  const resourceSize = parseInt(response.headers.get('Content-Length'), 10)
  let res = []
  async function read(reader, totalChunkSize = 0) {
    const { value = {}, done } = await reader.read()
    if (done) {
      store.state.search.viewProg = 0
      let r = Buffer.from(res)
      return JSON.parse(r.toString())
    }

    // push new bytes to res
    const length = value.byteLength
    for (let i = 0; i < length; i++) {
      res.push(value[i])
    }

    const percentComplete = Math.round(
      ((totalChunkSize + length) / resourceSize) * 100,
    )
    store.state.search.viewProg = percentComplete

    return await read(reader, totalChunkSize + length)
  }
  return await read(reader)
}

// convert string byte vals to binary
function str2ab(str) {
  let buf = new ArrayBuffer(str.length)
  var bufView = new Uint8Array(buf)
  buf = null
  for (var i = 0, strLen = str.length; i < strLen; i++) {
    bufView[i] = str.charCodeAt(i)
  }
  return bufView
}
