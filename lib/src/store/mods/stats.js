import { auth } from '@/api/http'

export default {
  namespaced: true,

  state: {
    stats: null,
  },

  mutations: {
    SET_STATS(state, stats) {
      state.stats = stats
    },
  },

  actions: {
    getStats: async function({ commit, dispatch }) {
      const stats = await auth.GET('stats').catch(e => {
        throw dispatch('util/notify', { e }, { root: true })
      })
      commit('SET_STATS', stats)
    },
  },
}
