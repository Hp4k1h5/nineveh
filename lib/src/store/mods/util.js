export default {
  namespaced: true,

  state: {
    notification: null,
  },

  mutations: {
    SET_NOTIFICATION(state, val) {
      state.notification = val
    },
  },

  actions: {
    notify({ commit }, notification) {
      if (notification.e) {
        notification = {
          msg: notification.e.toString(),
          color: '#ecab34',
        }
      }
      if (!notification.color) notification.color = '#9f9'
      commit('SET_NOTIFICATION', notification)
      setTimeout(() => commit('SET_NOTIFICATION', null), 2200)
    },

    sleep(undefined, ms) {
      return new Promise(resolve => setTimeout(resolve, ms))
    },
  },
}
