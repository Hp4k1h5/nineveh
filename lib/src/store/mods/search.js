import { auth } from '@/api/http'

export default {
  namespaced: true,

  state: {
    curDB: null,
    curCols: [],
    curLangs: ['en'],
    query: null,
    queryArr: [],
    filters: [],
    results: null,
    count: null,
    stats: null,
    querying: null,
    page: 0,
    rpp: 20,
    query_history: [],
    saved_queries: [],
    docView: null,
    viewProg: 0,
  },

  mutations: {
    SET_QUERY(state, query) {
      state.query = query
    },

    SET_QUERY_ARR(state, queryArr) {
      state.queryArr = queryArr
    },

    SET_QUERYING(state, querying) {
      state.querying = querying
    },

    SET_RESULTS(state, results) {
      state.results = results.results
      state.stats = results.stats
    },

    SET_SAVED(state, saved_queries) {
      state.saved_queries = saved_queries
    },

    SET_DOC(state, docView) {
      state.docView = docView
    },
  },

  actions: {
    async submit({ state, commit, dispatch }, append) {
      commit('SET_QUERYING', true)
      if (!append) state.page = 0
      const results = await auth
        .POST(`search/l/${state.curLangs[0]}`, {
          query: state.query,
          filters: state.filters,
          start: state.page * state.rpp,
          end: (state.page + 1) * state.rpp - state.page * state.rpp,
          queryArr: state.queryArr,
          collections: state.curCols,
          hl: true,
        })
        .catch(e => {
          dispatch('util/notify', { e }, { root: true })
        })
      commit('SET_QUERYING', results ? false : null)
      if (!results) return
      commit('SET_RESULTS', results)
    },

    async save({ state, commit, dispatch }) {
      const saved_queries = await auth
        .POST('search/save', state.queryArr)
        .catch(e => {
          dispatch('util/notify', { e }, { root: true })
        })
      if (!saved_queries) return
      commit('SET_SAVED', saved_queries)
      dispatch(
        'util/notify',
        { msg: 'query saved', color: '#9f9' },
        { root: true },
      )
    },

    async del({ state, commit, dispatch }, i) {
      const d = await auth.DELETE(`search/saved/${i}`).catch(e => {
        dispatch('util/notify', { e }, { root: true })
      })
      if (!d) return
      const r = [...state.saved_queries]
      r.splice(i, 1)
      commit('SET_SAVED', r)
      dispatch(
        'util/notify',
        { msg: 'query removed', color: '#f99' },
        { root: true },
      )
    },

    async view({ commit, dispatch }, _id) {
      commit('SET_DOC', { title: 'retrieving...' })
      const docView = await auth.GET(`view/${_id}`).catch(e => {
        dispatch('util/notify', { e }, { root: true })
        commit('SET_DOC', { title: 'err' })
      })
      if (!docView._id) return
      commit('SET_DOC', docView)
    },
  },
}
