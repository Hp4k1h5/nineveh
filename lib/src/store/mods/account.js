import { http, auth } from '@/api/http'

export default {
  namespaced: true,

  state: {
    jwt: null,
    name: null,
    email: null,
    dbs: null,
    cols: null,
    saved_queries: null,
  },

  mutations: {
    SET_USER(state, obj) {
      const { user, rootState } = obj
      state.jwt = user.jwt
      state.name = user.name || 'friend'
      state.email = user.email
      state.dbs = user.dbs
      if (rootState.settings.default_db) {
        rootState.search.curDB = rootState.settings.default_db
      } else {
        rootState.search.curDB = state.dbs[0].name
      }
      state.cols = user.dbs.find(
        db => db.name === rootState.search.curDB,
      ).collections
      rootState.search.curCols = state.cols.map(c => c.name)
      rootState.search.saved_queries = user.saved_queries
    },
  },

  actions: {
    async login({ state, rootState, commit, dispatch }, creds) {
      const user = await http
        .GET(`login?${new URLSearchParams(creds).toString()}`)
        .catch(e => {
          dispatch('util/notify', { e }, { root: true })
        })

      if (!user) return
      localStorage.setItem('jwt', user.jwt)
      auth.setAuth()
      commit('SET_USER', { user, rootState })
    },

    logout() {
      localStorage.removeItem('jwt')
      window.location.reload()
    },

    async refresh({ state, commit, dispatch, rootState }) {
      if (!localStorage.getItem('jwt')) {
        return
      }
      const user = await auth.POST('refresh').catch(e => {
        dispatch('util/notify', { e }, { root: true })
      })
      if (!user) {
        localStorage.removeItem('jwt')
        return
      }
      commit('SET_USER', { user, rootState })
      localStorage.setItem('jwt', state.jwt)
    },
  },
}
