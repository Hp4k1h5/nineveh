import Vue from 'vue'
import Vuex from 'vuex'

//import persistedSharedState from 'vuex-persisted-shared-state'

import util from '@/store/mods/util'
import account from '@/store/mods/account'
import settings from '@/store/mods/settings'
import search from '@/store/mods/search'
import stats from '@/store/mods/stats'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    util,
    account,
    settings,
    search,
    stats,
  },
  // plugins: [
  //   persistedSharedState({
  //     stats: 'SET_STATS',
  //   }),
  // ],
})
