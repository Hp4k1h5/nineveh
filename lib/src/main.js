import Vue from 'vue'

import { MdIcon } from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

import App from './App'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// set icons
Vue.use(MdIcon)
require('typeface-monda')

// instantiate the app
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
