import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import HomeSidebar from '@/components/sidebars/HomeSidebar'
import View from '@/views/View'
import About from '@/views/About'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    components: {
      main: Home,
      sidebar: HomeSidebar,
    },
  },
  {
    path: '/view/:col/:_key',
    name: 'view',
    components: {
      main: View,
      sidebar: HomeSidebar,
    },
    props: {
      main: true,
      sidebar: true,
    },
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    //component: () =>
    //  import(/* webpackChunkName: "about" */ '../views/About.vue'),
    components: {
      main: About,
      sidebar: HomeSidebar,
    },
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

export default router
