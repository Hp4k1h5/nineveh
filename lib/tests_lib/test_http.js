require('jsdom-global')()

const Vue = require('vue')
const VueTestUtils = require('@vue/test-utils')

import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import { assert, rek, apiAddress } from '../util/test_config.js'

import { http } from '../../../lib/src/api/http.js'
Vue.shallowMount(http)

export default [
  {
    name: 'http works ?',
    fn: async () => {
      assert.ok(http, 'no http')
    },
  },
]
