const fs = require('fs')

module.exports = {
  devServer: {
    host: 'localhost',
    port: 8080,
    open: true,
    disableHostCheck: true,
  },
}
