##### !!! Work in Progress (everything is under development)
## lib 
#### a Vue js based search interface
- contents
   - [usage](#usage)
   - [setup / development](#development-setup)
   - [deploying](#deploying)
   - [purpose](#purpose)

[nineveh](https://nineveh.now.sh) (hosted by [zeit](https://zeit.co))
- please contact [me](mailto:robertwalks@gmail.com) for beta credentials to log in
![homepage](public/img/lib_home-1.png)

### development setup
##### prerequisites:
 - a running ptolemy api server and arangodb server (see [ptolemy/README.md](https://gitlab.com/HP4k1h5/nineveh/blob/master/ptolemy/README.md))
##### dependencies
- [nodejs](https://nodejs.org/)
- [npm](https://www.npmjs.com/)/[yarn](https://yarnpkg.com/)
  - [vue-cli](https://www.npmjs.com/package/@vue/cli)
    - vuex
    - vue-router
    - core-js
  - [pdfjs-dist](https://www.npmjs.com/package/pdfjs-dist)
  - [vue-material](https://www.npmjs.com/package/vue-material)
  - typeface-monda

##### instructions:
- n.b. this project is hosted by [zeit's now](https://zeit.co) which provides automatic https, and therefore, this project is designed to run on port 80 in production, but will have all of its traffic encrypted, and expect all cors requests to be https as well, hence `npm run serve` will serve in https by default; for production: visit [certbot](https://certbot.eff.org/)

1) run `git clone https://gitlab.com/HP4k1h5/nineveh.git`
2) run `npm install` from the `lib` directory  
2) run `npm run serve` ,,also from the lib dir

### usage
Once the backend (ptolemy) is accessible at [localhost:3000](https://localhost:3000) (or wherever you have it set in `ptolemy-config.json`), and once you have [setup](#development-setup) this Vue project, you can login with default creds: 
```
user: test
password: test
```

### deploying
For production, run `npm run build`

### purpose
lib aims to be a cross-format, cross-language digital library, providing multi-language search and textual analysis.


