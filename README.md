#### !!! Work in Progress (everything is under development)

## nineveh

##### a document collection, storage, search, retrieval, and display information-system

![search](lib/public/img/search1.png)

###### see `README`S

- backend: [ptolemy](https://gitlab.com/HP4k1h5/nineveh/blob/master/ptolemy/README.md)
- frontend: [lib](https://gitlab.com/HP4k1h5/nineveh/blob/master/lib/README.md)
